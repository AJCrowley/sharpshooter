package ca.revolve.sharpshooter.elements
{
	public class Badge
	{
		public var id:String;
		public var behaviour:String;
		public var activated:Boolean = false;
	}
}