package ca.revolve.sharpshooter.constants
{
	public class FileConstants
	{
		// path to assets folder
		public static const ASSETS_DIR:String = "assets/";
		// filename for config file
		public static const CONFIG_FILE:String = "sharpshooter.config.xml";
	}
}