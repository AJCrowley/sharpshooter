package ca.revolve.sharpshooter.constants
{
	public class PuckModifierConstants
	{
		// puck behavioural modifier types
		public static const PERCENTAGE:String = "percent";
		public static const ABSOLUTE:String = "absolute";
	}
}